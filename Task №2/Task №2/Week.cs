﻿using System;
using System.Collections;

namespace Task__2
{
    public class Week : IEnumerable
    {
        WeekDay[] days = {
            new WeekDay("Понедельник"),
            new WeekDay("Вторник"),
            new WeekDay("Среда"),
            new WeekDay("Четверг"),
            new WeekDay("Пятница"),
            new WeekDay("Суббота"),
            new WeekDay("Воскресенье")
        };

        public IEnumerator GetEnumerator()
        {
            return new WeekEnumerator(days);
        }
    }
}
