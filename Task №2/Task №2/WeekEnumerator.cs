﻿using System;
using System.Collections;

namespace Task__2
{
    public class WeekEnumerator : IEnumerator
    {
        WeekDay[] day;
        int position = -1;
        public WeekEnumerator(WeekDay[] days)
        {
            day = days;
        }
        public object Current
        {
            get
            {
                if (position == -1 || position >= day.Length)
                    throw new InvalidOperationException();
                return day[position];
            }
        }

        public bool MoveNext()
        {
            if (position < day.Length - 1)
            {
                position++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            position = -1;
        }

    }
}
