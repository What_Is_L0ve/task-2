﻿using System;

namespace Task__2
{
    public class WeekDay
    {
        public WeekDay(string nameWeekDay)
        {
            NameWeekDay = nameWeekDay;
        }
        public string NameWeekDay { get; set; }
    }
}
