﻿using System;

namespace Task__2
{
    public class Program
    {
        static void Main(string[] args)
        {
            var week = new Week();

            foreach (var day in week)
            {
                Console.WriteLine($"День недели: {((WeekDay)day).NameWeekDay}");
            }
        }
    }
}
